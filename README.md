
# SG Utilities

SG Utilities is a generic level editor, written in python, using PySide and opengl.
It uses a tileset, but you are not limited to any grid when placing tiles.

![Screenshot][1]

## Planned features

  - Multiple tilesets
  - Animated tiles
  - Tiles in multiple layers
  - Parallax effect on different layers
  - Edit static collision geometry (separated from tiles)
  - Placement of game entities

[1]: http://cdn.bitbucket.org/thezic/sgutils/downloads/sg_screenhot.png
