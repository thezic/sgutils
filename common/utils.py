# This file is part of SG utilities.

# SG utilities is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG utilities is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG utilities.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

def override(interface_class):
    def override(method):
        assert(method.__name__ in dir(interface_class))
        return method
    return override