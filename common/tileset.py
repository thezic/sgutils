# This file is part of SG utilities.

# SG utilities is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG utilities is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG utilities.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

from PySide import QtCore
import json
import os

class NoTilesetLoaded(Exception):
    def __init__(self):
        super(NoTilesetLoaded, self).__init__('No tileset loaded')


class Tileset(QtCore.QObject):

    def __init__(self, path=None):
        super(Tileset, self).__init__()

        self.loaded = False

        if path:
            self.load_tileset_data(path)
            

    def load_tileset_data(self, path):
        self.path = os.path.abspath(path)
        self.file_root = os.path.dirname(self.path)        

        f = open(self.path)
        data = json.load(f)


        self.tileset_file = data[u'filename']

        self._tiles = {}
        for t in data[u'tiles']:
            self._tiles[t[u'id']] = QtCore.QRect(t['x'], t['y'], t['w'], t['h'])

        self.loaded = True


    def get_image_path(self):
        if not self.loaded:
            raise NoTilesetLoaded()

        return os.path.join(self.file_root, self.tileset_file)

    def tiles(self):
        for tid, rect in self._tiles.items():
            yield tid, rect

    def get_tile_rect(self, tid):
        return self._tiles[tid]

    def pixels_per_meter(self):
        return 32.0