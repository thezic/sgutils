# This file is part of SG Maped.

# SG Maped is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Maped is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Maped.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

import sys
import os
import math
from PySide.QtGui import *
from PySide.QtCore import Qt
from maped.gameview import GameView
from maped.TilesetScene import TilesetScene
from maped import maped_res
from gamemap import GameDocument
from common.tileset import Tileset


class SGMainWindow(QMainWindow):
    window_list = []

    def __init__(self):
        super(SGMainWindow, self).__init__()

        self.game_view = GameView()
        self.setCentralWidget(self.game_view)

        self.statusbar = QStatusBar(self)
        self.setStatusBar(self.statusbar)

        self.tileset_scene = TilesetScene(self)

        self.create_actions() 
        self.create_dockwidgets() ### WARNING this also creates actions
        self.create_menus()
        self.create_toolbar()

        self.tileset_scene.selectionChanged.connect(self.set_tile)
        self.setFocusPolicy(Qt.StrongFocus)

        self.create_document()
        self.resize(1024, 768)
        self.show()

    def create_actions(self):
        self.actions = {}

        openAct =  QAction(QIcon(":/images/open.png"), self.tr("&Open..."), self)
        openAct.setShortcuts(QKeySequence.Open)
        openAct.setStatusTip(self.tr("Open an existing map"))
        openAct.triggered.connect(self.open_map)
        self.actions['open'] = openAct

        saveAct = QAction(QIcon(':/images/save.png'), self.tr('&Save'), self)
        saveAct.setShortcuts(QKeySequence.Save)
        saveAct.setStatusTip(self.tr('Save map to file'))
        saveAct.triggered.connect(self.save_map)
        self.actions['save'] = saveAct

        newAct = QAction(QIcon(':/images/new.png'), self.tr('&New map'), self)
        newAct.setShortcuts(QKeySequence.New)
        newAct.setStatusTip(self.tr('Create a new map'))
        newAct.triggered.connect(self.new_map)
        self.actions['new'] = newAct

        tsAct = QAction(QIcon(':/images/large_tiles.png'), self.tr('&Load Tileset...'), self)
        tsAct.setStatusTip(self.tr('Load a new tileset'))
        tsAct.triggered.connect(self.load_tileset)
        self.actions['load_tileset'] = tsAct

        # View tools
        toolZoom_in = QAction(QIcon(':/images/zoom_in.png'), self.tr('&Zoom in'), self)
        toolZoom_in.setStatusTip(self.tr('Zoom in'))
        toolZoom_in.triggered.connect(self.zoom_in)
        self.actions['zoom_in'] = toolZoom_in

        toolZoom_out = QAction(QIcon(':/images/zoom_out.png'), self.tr('&Zoom out'), self)
        toolZoom_out.setStatusTip(self.tr('Zoom out'))
        toolZoom_out.triggered.connect(self.zoom_out)
        self.actions['zoom_out'] = toolZoom_out

        # Tile tools
        toolDeleteTile = QAction(QIcon(':/images/pencil_delete.png'), self.tr('&Delete tile'), self)
        toolDeleteTile.setShortcuts(QKeySequence.Delete)
        toolDeleteTile.setStatusTip(self.tr('Delete tile'))
        toolDeleteTile.triggered.connect(self.game_view.delete_selected_tile)
        self.actions['delete'] = toolDeleteTile

        toolModeGroup = QActionGroup(self)
        toolModeGroup.triggered.connect(self.set_tool)

        toolAddMode = QAction(QIcon(':/images/pencil_add.png'), self.tr('&Add mode'), self)
        toolAddMode.setActionGroup(toolModeGroup)
        toolAddMode.setStatusTip(self.tr('Add items mode'))
        toolAddMode.setCheckable(True)
        toolAddMode.setChecked(True)
        toolAddMode.setData('add')

        toolEditMode = QAction(QIcon(':/images/select.png'), self.tr('&Edit mode'), self)
        toolEditMode.setActionGroup(toolModeGroup)
        toolEditMode.setStatusTip(self.tr('Edit items mode'))
        toolEditMode.setCheckable(True)
        toolEditMode.setData('edit')
        self.actions['tool_mode_group'] = toolModeGroup

    def create_menus(self):
        file_menu = self.menuBar().addMenu(self.tr("&File"))
        file_menu.addAction(self.actions['new'])
        file_menu.addAction(self.actions['open'])

        view_menu = self.menuBar().addMenu(self.tr('&View'))
        view_menu.addAction(self.actions['toggle_tileset'])

        game_menu = self.menuBar().addMenu(self.tr('&Game'))
        game_menu.addAction(self.actions['load_tileset'])

    def create_toolbar(self):
        toolbar = self.addToolBar(self.tr("File"))
        toolbar.addAction(self.actions['new'])
        toolbar.addAction(self.actions['save'])
        toolbar.addAction(self.actions['open'])

        game = self.addToolBar(self.tr("Game"))
        game.addAction(self.actions['load_tileset'])

        tools = self.addToolBar(self.tr("Tools"))
        tools.addActions(self.actions['tool_mode_group'].actions())
        tools.addSeparator()
        tools.addAction(self.actions['delete'])

        view = self.addToolBar(self.tr("View"))
        view.addAction(self.actions['zoom_in'])
        view.addAction(self.actions['zoom_out'])

    def create_dockwidgets(self):
        tileset_dock = QDockWidget(self.tr("Tileset"), self)
        self.addDockWidget(Qt.BottomDockWidgetArea, tileset_dock)
        self.actions['toggle_tileset'] = tileset_dock.toggleViewAction()

        tileset_view = QGraphicsView(self.tileset_scene, tileset_dock)
        tileset_dock.setWidget(tileset_view)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_W:
            self.game_view.start_panning('up')

        elif event.key() == Qt.Key_S:
            self.game_view.start_panning('down')

        elif event.key() == Qt.Key_A:
            self.game_view.start_panning('left')

        elif event.key() == Qt.Key_D:
            self.game_view.start_panning('right')

        else:
            return super(SGMainWindow, self).keyPressEvent(event)

    def keyReleaseEvent(self, event):
        if event.key() == Qt.Key_W:
            self.game_view.stop_panning('up')

        elif event.key() == Qt.Key_S:
            self.game_view.stop_panning('down')

        elif event.key() == Qt.Key_A:
            self.game_view.stop_panning('left')

        elif event.key() == Qt.Key_D:
            self.game_view.stop_panning('right')

        else:
            return super(SGMainWindow, self).keyReleaseEvent(event)

    def open_map(self):
        filename, file_filter = QFileDialog.getOpenFileName(self)

        if filename:
            self.create_document()
            self.document.load(filename)

            # Load tileset
            for tsid, tileset in self.document.tilesets():
                path = os.path.abspath(
                        os.path.join(os.path.dirname(filename), tileset))
                self.load_tileset(path)

    def create_document(self):
        self.document = GameDocument()
        self.game_view.set_document(self.document)

    def new_map(self):        
        other = SGMainWindow()
        SGMainWindow.window_list.append(other)
        other.move(self.x() + 40, self.y() + 40)

    def save_map(self):
        if self.document.filename:
            filename = self.document.filename
        else:
            filename, filter_string = QFileDialog.getSaveFileName(self)

        self.document.save(filename)

    def load_tileset(self, filename=None):
        if not filename:
            #filename, file_filter = QFileDialog.getOpenFileName(self)
            filename = '/Users/simon/Dev/game/sgame/resources/tilesets/rebecka.json'

        if filename:
            self.current_tileset = Tileset(filename)
            self.tileset_scene.load_tileset(self.current_tileset)
            self.game_view.load_tileset(self.current_tileset)

            self.document.add_tileset(filename)

    def set_tile(self):
        self.current_tile = self.tileset_scene.get_current_tile()
        self.game_view.set_tile(self.current_tile)
        print "set tile", self.current_tile

    def set_tool(self, action):
        mode = action.data()
        self.game_view.set_tool_mode(mode)

    def zoom_in(self):
        self.game_view.zoom_in(10)

    def zoom_out(self):
        self.game_view.zoom_out(10)

class SGMapEditorApp(QApplication):
    def __init__(self):
        super(SGMapEditorApp, self).__init__(sys.argv)

        self.setApplicationName("SG Level Editor")
        main_window = SGMainWindow()
        

        sys.exit(self.exec_())

def main():
    SGMapEditorApp()