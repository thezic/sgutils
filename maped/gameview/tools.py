# This file is part of SG Maped.

# SG Maped is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Maped is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Maped.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

from PySide.QtCore import Qt

class Tool(object):
    def mouseMoveEvent(self, point):
        self.mouse_pos = point


class TileTool(Tool):
    def __init__(self, widget):
        self.widget = widget
        self.theta = 0
        self.mode = 'add'
        self.current_tile = None
        self.tsid = 1
        self.mdown_pos = None

    def mouseMoveEvent(self, point, event):
        super(TileTool, self).mouseMoveEvent(point)
        if self.mode == 'add' and self.current_tile is not None:
            self.widget.updateGL()

        if event.buttons() & Qt.MouseButton.LeftButton:
            self.mouseLDrag(point, event)
        elif event.buttons() & Qt.MouseButton.RightButton:
            self.mouseRDrag(point, event)

    def mouseLDrag(self, point, event):
        if self.mode == 'edit':
            # move tile
            dx = point.x - self.last_mpos.x
            dy = point.y - self.last_mpos.y

            for tile in self.widget.selected_tiles:
                tile.x += dx
                tile.y += dy
            self.widget.updateGL()

        self.last_mpos = point


    def mouseRDrag(self, point, event):
        print 'rdrag'

    def mouseReleaseEvent(self, point, event):
        self.mdown_pos = None
        self.last_mpos = None

    def mousePressEvent(self, point, event):
        self.mdown_pos = point
        self.last_mpos = point
        if self.mode == 'add':
            if self.current_tile is None:
                return

            self.widget.document.tilemap.insert(1, self.tsid, 
                    self.current_tile, 
                    point.x, point.y, 
                    self.theta)

        elif self.mode == 'edit':
            if event.modifiers() & Qt.KeyboardModifier.ControlModifier:
                append = True
            else:
                append = False
            self.widget.select_tile(point, append)

        self.widget.updateGL()

    def wheelEvent(self, event):
        if self.mode == 'add':
            degrees = event.delta() / 8.0
            self.theta += degrees
            self.widget.updateGL()

    def draw(self):
        if self.mode == 'add':
            #Draw current tile
            if self.current_tile is not None:
                ts = self.widget.tileset[self.tsid]
                ts.draw_tile(
                        self.current_tile,
                        self.mouse_pos.x,
                        self.mouse_pos.y,
                        self.theta)
