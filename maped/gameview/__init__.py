# This file is part of SG Maped.

# SG Maped is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Maped is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Maped.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

from PySide.QtCore import Qt
from PySide.QtCore import QRectF, QPointF
from PySide import QtCore
from PySide import QtGui
from PySide.QtOpenGL import QGLWidget
from OpenGL.GL import *
from math import sin, cos, radians
from collections import namedtuple
from common.utils import override

from .tools import TileTool

Point = namedtuple('Point', ['x', 'y'])

OBB = namedtuple('OBB', ['x', 'y', 'w', 'h', 'theta'])

def rotate_point(p, c, theta, ccw=False):
        fi = radians(theta)
        # print sin(fi), cos(fi)

        if not ccw:
            x =  cos(fi) * (p.x - c.x) - sin(fi) * (p.y - c.y)
            y =  sin(fi) * (p.x - c.x) + cos(fi) * (p.y - c.y)
        else:
            x =  cos(fi) * (p.x - c.x) + sin(fi) * (p.y - c.y)
            y = -sin(fi) * (p.x - c.x) + cos(fi) * (p.y - c.y)
        
        return Point(x, y)

class OBB(OBB):
    def check_point(self, point):
        pp = rotate_point(point, Point(self.x, self.y), self.theta, ccw=True)

        x = pp.x
        y = pp.y

        hw = self.w / 2.0
        hh = self.h / 2.0

        #inside?
        if y > -hh and y < hh and x > -hw and x < hw:
            return True
        else:
            return False

    def corners(self):
        print self.theta
        x1 = self.x - self.w/2
        y1 = self.y - self.h/2
        x2 = self.x + self.w/2
        y2 = self.y + self.h/2

        cp = Point(self.x, self.y)
        p1 = rotate_point(Point(x1, y1), cp, self.theta)
        p2 = rotate_point(Point(x2, y1), cp, self.theta)
        p3 = rotate_point(Point(x2, y2), cp, self.theta)
        p4 = rotate_point(Point(x1, y2), cp, self.theta)

        return p1, p2, p3, p4

class GLTileset(object):
    def __init__(self, tileset, glWidget):
        self._tileset = tileset
        self._glWidget = glWidget

        self._load_texture()

    def get_rect(self, tid):
        rect = QRectF(self._tileset.get_tile_rect(tid))
        rect.setWidth(rect.width() / self._tileset.pixels_per_meter())
        rect.setHeight(rect.height() / self._tileset.pixels_per_meter())
        rect.moveCenter(QPointF(0, 0))

        return rect

    def _load_texture(self):
        pixmap = QtGui.QPixmap(self._tileset.get_image_path())

        self._texture = self._glWidget.bindTexture(pixmap)
        self._tex_width = pixmap.width()
        self._tex_height = pixmap.height()

    def draw_tile(self, tid, x, y, theta=0):
        rect = self._tileset.get_tile_rect(tid)

        wm = rect.width() / (2.0 * self._tileset.pixels_per_meter())
        hm = rect.height() / (2.0 * self._tileset.pixels_per_meter())

        glPushMatrix()
        # Draw tileset image
        glEnable(GL_TEXTURE_2D)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        glBindTexture(GL_TEXTURE_2D, self._texture)

        glTranslate(x, y, 0)
        glRotate(theta, 0, 0, 1)

        glBegin(GL_QUADS)
        glColor(1,1,1)
        glTexCoord2d(*self._convert_to_texcoords(rect.left(), rect.bottom()))
        glVertex(-wm, -hm)

        glTexCoord2d(*self._convert_to_texcoords(rect.right(), rect.bottom()))
        glVertex(wm, -hm)

        glTexCoord2d(*self._convert_to_texcoords(rect.right(), rect.top()))
        glVertex(wm , hm)

        glTexCoord2d(*self._convert_to_texcoords(rect.left(), rect.top()))
        glVertex(-wm, hm)

        glEnd()

        glDisable(GL_TEXTURE_2D)

        glPopMatrix()

    def draw_selection(self, tid, x, y, theta=0):
        """
        draw selection box with current transformation
        """
        rect = self._tileset.get_tile_rect(tid)

        wm = rect.width() / (2.0 * self._tileset.pixels_per_meter())
        hm = rect.height() / (2.0 * self._tileset.pixels_per_meter())

        glPushMatrix()
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        # Draw selections box
        glTranslate(x, y, 0)
        glRotate(theta, 0, 0, 1)

        glBegin(GL_QUADS)
        glColor(1,1,0)
        glVertex(-wm, -hm)
        glVertex(wm, -hm)
        glVertex(wm , hm)
        glVertex(-wm, hm)
        glEnd()
        glPopMatrix()


    def _convert_to_texcoords(self, x, y):
        """
        Convert a coordinate in tileset image coords to a texture coord

        returns tuple (x, y)
        """
        x = float(x) / float(self._tex_width)
        y = 1 - float(y) / float(self._tex_height)
        return Point(x, y)


class GameView(QGLWidget):

    def __init__(self, *args, **kwargs):
        super(GameView, self).__init__(*args, **kwargs)

        self.tools = {
            'tile': TileTool(self)
        }
        self.tool = self.tools['tile']
        self.tileset = {}

        self.setMouseTracking(True)
        self.scale = 20
        self.pan_x = 0
        self.pan_y = 0
        self.selected_tiles = []

        self.timer = QtCore.QTimer(self)
        self.timer.start(25)
        self.timer.timeout.connect(self.update)

        self._panning = {
            'up': {'c': None, 'm': 0},
            'down': {'c': None, 'm': 0},
            'left': {'c': None, 'm': 0},
            'right': {'c': None, 'm': 0},
        }

    @override(QGLWidget)
    def initializeGL(self):
        print "Initialize GL"

        # set viewing projection
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClearDepth(1.0)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    @override(QGLWidget)
    def resizeGL(self, w, h):
        glViewport(0, 0, w, h)
        glMatrixMode(GL_PROJECTION)

        glLoadIdentity()
        glOrtho(0, w, 0, h, 1, -1)


    @override(QGLWidget)
    def paintGL(self):
        glMatrixMode(GL_MODELVIEW)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()

        glTranslate(self.pan_x, self.pan_y, 0)
        glScale(self.scale, self.scale, 1)

        if self.tileset:
            self._draw_document()


        self.tool.draw()

        glFlush()

    def update(self):
        # Update panning
        for direction, value in self._panning.items():
            if value['c'] == 'pan':
                value['m'] = 5.0

            if direction == 'left':
                self.pan_x += value['m']

            elif direction == 'right':
                self.pan_x -= value['m']

            elif direction == 'up':
                self.pan_y -= value['m']

            elif direction == 'down':
                self.pan_y += value['m']

            value['m'] *= 0.8
            if value['m'] < 0.1:
                value['m'] = 0

        self.updateGL()

    def _draw_document(self):
        glPushMatrix()

        for tile in self.document.tilemap.tiles():
            ts = self.tileset[tile.tsid]
            ts.draw_tile(tile.tid, tile.x, tile.y, tile.theta)

        # Draw selection
        for tile in self.selected_tiles:
            ts = self.tileset[tile.tsid]
            ts.draw_selection(tile.tid, tile.x, tile.y, tile.theta)

            # glPushMatrix()
            # glDisable(GL_BLEND)
            # obb = self.get_tile_obb(tile)
            # p1, p2, p3, p4 = obb.corners()

            # glTranslate(tile.x, tile.y, 0)
            # glBegin(GL_QUADS)
            # glVertex(p1.x, p1.y)
            # glVertex(p2.x, p2.y)
            # glVertex(p3.x, p3.y)
            # glVertex(p4.x, p4.y)
            # glEnd()
            # glPopMatrix()
        glPopMatrix()

    override(QGLWidget)
    def mousePressEvent(self, event):
        self.tool.mousePressEvent(self._to_gl_coord(event.x(), event.y()), event)

    def mouseMoveEvent(self, event):
        self.tool.mouseMoveEvent(self._to_gl_coord(event.x(), event.y()), event)

    def wheelEvent(self, event):
        self.tool.wheelEvent(event)

    def start_panning(self, direction):
        self._panning[direction]['c'] = 'pan'

    def stop_panning(self, direction):
        self._panning[direction]['c'] = None

    def _to_gl_coord(self, x, y):
            x = (x - self.pan_x) / float(self.scale)
            y = (self.height() - y - self.pan_y) / float(self.scale)
            return Point(x, y)

    def get_tile_obb(self, tile):
        """
        Return oriented bounding box for tile, expressed in gl_coords
        """
        tileset = self.tileset[tile.tsid]
        tr = tileset.get_rect(tile.tid)

        obb = OBB(tile.x, tile.y, tr.width(), tr.height(), tile.theta)
        return obb
        

    def load_tileset(self, tileset):
        self.tileset[1] = GLTileset(tileset, self)
        self.updateGL()

    def set_document(self, doc):
        self.document = doc

    def set_tile(self, tid):
        self.tools['tile'].current_tile = tid

    def set_tool_mode(self, mode):
        self.tool.mode = mode

    def delete_selected_tile(self):
        for tile in self.selected_tiles:
            self.document.tilemap.remove(tile)

        self.selected_tiles = []
        self.updateGL()

    def select_tile(self, point, append=False):
        for tile in self.document.tilemap.tiles(reverse=True):
            obb = self.get_tile_obb(tile)
            if obb.check_point(point):
                break
        else:
            # No tile found
            if not append:
                self.selected_tiles = []
            return

        # Tile found, select or deselect it
        if append:
            if tile in self.selected_tiles:
                # Already selected, remove from selection instead
                self.selected_tiles.remove(tile)
            else:
                self.selected_tiles.append(tile)
        else:
            self.selected_tiles = [tile]

    def zoom_in(self, step):
        value = step / 100.0
        self.scale = self.scale * (1 + value)
        self.updateGL()

    def zoom_out(self, step):
        value = (100 - step) / 100.0
        self.scale = self.scale * value
        self.updateGL()