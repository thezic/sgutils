# This file is part of SG Maped.

# SG Maped is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Maped is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Maped.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

from PySide import QtCore
from PySide import QtGui

from collections import namedtuple
from xml.etree import ElementTree as ET
from xml.dom import minidom as md
import os

class TileItem(object):
    def __init__(self, tsid, tid, x, y, theta):
        self.tsid = tsid
        self.tid = tid
        self.x = x
        self.y = y
        self.theta = theta

    def serialize(self, node):
        """
        Serialize tile
        """
        tile_node = ET.SubElement(node, 'tile')

        tile_node.set('tsid', str(self.tsid))
        tile_node.set('tid', str(self.tid))
        tile_node.set('x', str(self.x))
        tile_node.set('y', str(self.y))
        tile_node.set('theta', str(self.theta))

class TileMap(object):
    def __init__(self):
        self._layers = {}

    def insert(self, layer, tsid, tid, x, y, theta=0):
        try:
            layer_list = self._layers[layer]
        except KeyError:
            layer_list = self._layers[layer] = []

        layer_list.append(TileItem(tsid, tid, x, y, theta))

    def remove(self, tile):
        # A tile can only be in one layer
        for layer in self._layers.values():
            layer.remove(tile)

    def tiles(self, layer=None, reverse=False):
        if layer:
            layers = [layers[layer]]
        else:
            layers = self._layers.keys()
            if not reverse:
                layers.reverse()

        for layer in layers:
            tiles = list(self._layers[layer])
            if reverse:
                tiles.reverse()
            for tile in tiles:
                yield tile

    def serialize(self, node):
        """
        Serialize tilemap to an etree node
        """
        for layer, tiles in self._layers.items():
            layer_node = ET.SubElement(node, 'layer')
            layer_node.set('id', str(layer))

            for tile in tiles:
               tile.serialize(layer_node)

    def load(self, node):
        """
        Load tilemap freom etree node
        """
        for n in node.iter():
            if n.tag == "layer":
                layer = int(n.get("id"))
            elif n.tag == "tile":
                tsid = int(n.get('tsid'))
                tid = int(n.get('tid'))
                x = float(n.get('x'))
                y = float(n.get('y'))
                theta = float(n.get('theta'))

                self.insert(layer, tsid, tid, x, y, theta)


class GameDocument(QtCore.QObject):
    def __init__(self):
        print 'Initialize Game map'
        self._tilemap = TileMap()
        self._tilesets = {}
        self._filename = None

    @property
    def filename(self):
        return self._filename

    @property
    def tilemap(self):
        return self._tilemap

    def tilesets(self):
        for tsid, ts in self._tilesets.items():
            yield tsid, ts

    def add_tileset(self, path):
        self._tilesets[1] = path

    def load(self, filename):
        self._filename = filename
        tree = ET.ElementTree()
        tree.parse(filename)

        # Tilesets
        tilesets_node = tree.find('tilesets')
        for tsn in tilesets_node.iter('tileset'):
            self._tilesets[int(tsn.get('id'))] = tsn.text.strip()

        # Tilemap
        tilemap_node = tree.find('tilemap')
        self.tilemap.load(tilemap_node)

    def save(self, filename):
        file_dir = os.path.dirname(filename)

        root = ET.Element('map')

        tilesets = ET.SubElement(root, 'tilesets')
        for tsid, path in self._tilesets.items():
            relpath = os.path.relpath(path, file_dir)
            ts_node = ET.SubElement(tilesets, 'tileset')
            ts_node.set('id', str(tsid))
            ts_node.text = relpath

        tilemap = ET.SubElement(root, 'tilemap')
        self.tilemap.serialize(tilemap)

        # Reparse as minidom
        dom = md.parseString(ET.tostring(root))
        # write pretty xml

        with open(filename, 'w') as f:
            dom.writexml(f, indent='', addindent='    ', newl='\n')

        self._filename = filename
