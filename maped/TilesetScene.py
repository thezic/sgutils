# This file is part of SG Maped.

# SG Maped is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Maped is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Maped.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

from PySide import QtGui
from PySide.QtCore import Qt, QRect

class TileItem(QtGui.QGraphicsPixmapItem):
    def __init__(self, *args, **kwargs):
        self.tid = kwargs.pop('tid')

        super(TileItem, self).__init__(*args, **kwargs)
        self.setFlag(self.ItemIsSelectable)


class TilesetScene(QtGui.QGraphicsScene):        

    def reposition_items(self):
        """
        Reposition all items
        """
        width = min([v.width() for v in self.views()])

        x = y = 0
        row_height = 0
        for item in self.items():
            item_width = item.boundingRect().width()
            item_height = item.boundingRect().height()

            if row_height < item_height:
                row_height = item_height

            item.setPos(x, y)
            if x > 0 and x + item_width > width:
                x = 0
                y = y + row_height

                row_height = 0

            else:
                x = x + item_width

    def load_tileset(self, tileset):
        """
        Load a tileset
        """
        self.clear()
        tileset_pm = QtGui.QPixmap(tileset.get_image_path())

        painter = QtGui.QPainter()

        for tid, rect in tileset.tiles():
            tile_pm = QtGui.QPixmap(rect.width(), rect.height())
            tile_pm.fill(Qt.transparent)

            dst_rect = QRect(0, 0, rect.width(), rect.height())
            painter.begin(tile_pm)
            painter.drawPixmap(dst_rect, tileset_pm, rect)
            painter.end()

            tile = TileItem(tile_pm, tid=tid)

            self.addItem(tile)

        self.reposition_items()

    def get_current_tile(self):
        try:
            return self.selectedItems()[0].tid
        except IndexError:
            return None

