# This file is part of SG Tiled.

# SG Tiled is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Tiled is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Tiled.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

from OpenGL.GL import *

class Tile(object):
    next_id = 0

    def __init__(self, p1, p2):
        self.left, self.top = p1
        self.right, self.bottom = p2

        if self.left > self.right or self.top > self.bottom:
            self.left, self.right = self.right, self.left
            self.top, self.bottom = self.bottom, self.top

        self.id = Tile.next_id
        Tile.next_id += 1

        print self

    @property
    def width(self):
        return self.right - self.left

    @property
    def height(self):
        return self.bottom - self.top


    def __str__(self):
        return ', '.join(map(str, [self.left, self.top, self.right, self.bottom]))

    def draw(self):
        glPushMatrix()

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        glBegin(GL_QUADS)
        glColor(1, 0, 0, 0.5)
        glVertex(self.left, self.top)
        glVertex(self.right, self.top)
        glVertex(self.right, self.bottom)
        glVertex(self.left, self.bottom)
        glEnd()

        glPopMatrix()


class Tileset(object):
    def __init__(self):
        self.tiles = []

    def add_tile(self, tile):
        self.tiles.append(tile)

    def find_tile(self, x, y):
        for tile in self.tiles:
            if tile.left < x < tile.right and tile.top < y < tile.bottom:
                return tile
        return None

    def draw(self):
        for tile in self.tiles:
            tile.draw()