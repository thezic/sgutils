# This file is part of SG Tiled.

# SG Tiled is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Tiled is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Tiled.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

from PySide import QtGui, QtCore
from PySide.QtOpenGL import QGLWidget
from OpenGL.GL import *
from tileset import Tile, Tileset
from common.utils import override

TILESET = "/Users/simon/Dev/game/sgame/resources/tilesets/rebecka.png"

ACT_CREATE_TILE = 'create'
ACT_MODIFY_TILE = 'modify'
ACT_MOVE_TILE = 'move'

class TilesetWidget(QGLWidget):
    tileAdded = QtCore.Signal(Tile)

    def __init__(self, *args, **kwargs):
        super(TilesetWidget, self).__init__(*args, **kwargs)

        self.is_selecting = False
        self.tileset = Tileset()
        self.active_tile = None
        self.current_action = None

    def load_tileset_image(self, filename):
        # Load tileset
        tileset = QtGui.QPixmap(filename)
        self.tileset_texture = self.bindTexture(tileset)

        self.tileset_width = float(tileset.rect().width())
        self.tileset_height = float(tileset.rect().height())
        self.aspect_ratio = self.tileset_width / self.tileset_height
        
        print self.aspect_ratio


        print tileset.rect().width(), tileset.rect().height()

    
    @override(QGLWidget)
    def initializeGL(self):
        glClearColor(0.0, 1.0, 0.0, 0.0)
        glClearDepth(1.0)

        self.load_tileset_image(TILESET)

        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        #glEnable(GL_DEPTH_TEST)
        #glEnable(GL_CULL_FACE)


    @override(QGLWidget)
    def resizeGL(self, w, h):
        glViewport(0, 0, w, h)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        glOrtho(0, w, h, 0, 1, -1)

    @override(QGLWidget)
    def paintGL(self):
        glMatrixMode(GL_MODELVIEW)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()

        # Draw tileset image
        glEnable(GL_TEXTURE_2D)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        glBindTexture(GL_TEXTURE_2D, self.tileset_texture)
        glBegin(GL_QUADS)
        glColor(1, 1, 1)

        glTexCoord2d(0, 1)
        glVertex(0, 0)

        glTexCoord2d(0, 0)
        glVertex(0, self.tileset_height)

        glTexCoord2d(1, 0)
        glVertex(self.tileset_width, self.tileset_height)        
 
        glTexCoord2d(1, 1)
        glVertex(self.tileset_width, 0)

        glEnd()
        glDisable(GL_TEXTURE_2D)

        self.draw_tiles()

        # Draw Selection
        if self.is_selecting:
            self.draw_selection()

    def draw_tiles(self):
        self.tileset.draw()

    def draw_selection(self):
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        glPushMatrix()

        glBegin(GL_QUADS)
        glColor(1, 1, 1, 0.5)

        glVertex(self.mouse_x1, self.mouse_y1)
        glVertex(self.mouse_x2, self.mouse_y1)
        glVertex(self.mouse_x2, self.mouse_y2)
        glVertex(self.mouse_x1, self.mouse_y2)

        glEnd()

        glPointSize(10);
        glBegin(GL_POINTS)
        glColor(1, 1, 1, 1)

        glVertex(self.mouse_x1, self.mouse_y1)
        glVertex(self.mouse_x2, self.mouse_y1)
        glVertex(self.mouse_x2, self.mouse_y2)
        glVertex(self.mouse_x1, self.mouse_y2)

        glEnd()

        glPopMatrix()


    @override(QGLWidget)
    def mousePressEvent(self, event):
        self.mouse_x1 = event.x()
        self.mouse_y1 = event.y()


    @override(QGLWidget)
    def mouseReleaseEvent(self, event):
        self.mouse_x2 = event.x()
        self.mouse_y2 = event.y()

        if self.mouse_x1 == self.mouse_x2 and self.mouse_y1 == self.mouse_y2:
            self.on_click(event.x(), event.y())

        else:
            self.end_select(event.x(), event.y())


    @override(QGLWidget)
    def mouseMoveEvent(self, event):
        self.mouse_x2 = event.x()
        self.mouse_y2 = event.y()

        if self.current_action == None:
            self.start_select()

        if self.current_action == ACT_CREATE_TILE:
            self.updateGL()

    def on_click(self, x, y):
        if self.current_action != None:
            return

        tile = self.tileset.find_tile(x, y)

        if self.active_tile:
            self.active_tile.is_active = False

        if tile:
            self.active_tile = tile
            tile.is_active = True
        else:
            self.active_tile = None

        print tile


    def start_select(self):
        self.current_action = ACT_CREATE_TILE
        self.is_selecting = True


    def update_selection(self, x, y):
        self.mouse_x2 = x
        self.mouse_y2 = y


    def end_select(self, x, y):
        self.is_selecting = False

        x1 = self.mouse_x1
        y1 = self.mouse_y1
        x2 = self.mouse_x2
        y2 = self.mouse_y2

        if x1 < 0:
            x1 = 0
        if x1 > self.tileset_width:
            x1 = self.tileset_width

        if x2 < 0:
            x2 = 0
        if x2 > self.tileset_width:
            x2 = self.tileset_width

        if y1 < 0:
            y1 = 0
        if y1 > self.tileset_height:
            y1 = self.tileset_height

        if y2 < 0:
            y2 = 0
        if y2 > self.tileset_height:
            y2 = self.tileset_height

        p1 = (x1, y1)
        p2 = (x2, y2)

        tile = Tile(p1, p2)
        self.tileset.add_tile(tile)
        self.tileAdded.emit(tile)

        self.current_action = None

        self.updateGL()