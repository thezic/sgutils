# This file is part of SG Tiled.

# SG Tiled is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Tiled is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Tiled.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

import sys

from PySide import QtGui, QtCore
from PySide.QtOpenGL import QGLWidget
from OpenGL.GL import *

from tileset import Tileset, Tile
from tilesetwidget import TilesetWidget


class TileListTree(QtGui.QTreeWidget):
    def __init__(self, *args, **kwargs):
        super(TileListTree, self).__init__(*args, **kwargs)

        self.setColumnCount(5)
        self.setHeaderLabels([
                'ID', 'Left', 'Top', 'Width', 'Height'
            ])

        self.next_id = 0

    @QtCore.Slot(Tile)
    def addTile(self, tile):
        item = QtGui.QTreeWidgetItem(self)
        
        item.setText(0, str(tile.id))
        item.setText(1, str(tile.left))
        item.setText(2, str(tile.top))
        item.setText(3, str(tile.width))
        item.setText(4, str(tile.height))

        self.next_id = self.next_id + 1
        print "tile added:", tile


class SGTilesetEditor(QtGui.QMainWindow):
    def __init__(self):
        super(SGTilesetEditor, self).__init__()
        
        self.splitter = QtGui.QSplitter(self)

        self.tileset_widget = TilesetWidget(self)

        self.tile_list_widget = TileListTree(self)

        self.tileset_widget.tileAdded.connect(self.tile_list_widget.addTile)


        self.splitter.addWidget(self.tile_list_widget)
        self.splitter.addWidget(self.tileset_widget)
        self.splitter.setSizes([30, 300])

        self.setCentralWidget(self.splitter)
        self.resize(800, 600)
        self.show()


def main():
    app = QtGui.QApplication(sys.argv)

    app.setApplicationName('SG Tileset Editor')

    window = SGTilesetEditor()
    sys.exit(app.exec_())