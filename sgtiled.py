#!/usr/bin/env python2.7
# This file is part of SG Tiled.

# SG Tiled is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SG Tiled is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SG Tiled.  If not, see <http://www.gnu.org/licenses/>.

# (C) copyright 2012 - Simon Dahlberg. All Rights reserved.

from tiled import main

if __name__ == '__main__':
    main()
